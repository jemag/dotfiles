vim.g.nvim_tree_indent_markers = 1 -- 0 by default, this option shows indent markers when folders are open
require("nvim-tree").setup({
	disable_netrw = true,
	hijack_netrw = true,
	open_on_setup = false,
	ignore_ft_on_setup = { "startify", "dashboard" },
	auto_close = true,
	open_on_tab = false,
	hijack_cursor = false,
	update_cwd = false,
	diagnostics = {
		enable = true,
	},
	update_focused_file = {
		enable = false,
		update_cwd = false,
		ignore_list = {},
	},
	system_open = {
		cmd = nil,
		args = {},
	},
	view = {
		width = 65,
		side = "left",
		auto_resize = false,
		mappings = {
			custom_only = false,
			list = {
				{ key = "X", cb = ':lua require"nvim-tree.lib".collapse_all()<CR>' },
			},
		},
	},
})
